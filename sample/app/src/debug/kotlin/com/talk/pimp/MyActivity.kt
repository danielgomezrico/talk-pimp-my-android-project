package com.talk.pimp

import android.support.annotation.LayoutRes
import android.view.View
import com.talk.pimp.base.BaseActivity

open class MyActivity : BaseActivity() {

  override fun setContentView(view: View?) {
    super.setContentView(view)
  }

  override fun setContentView(@LayoutRes layout: Int) {
    super.setContentView(layout)
    initDebugDrawer()
  }

  private fun initDebugDrawer() {
    // Init your debug drawer
  }

}
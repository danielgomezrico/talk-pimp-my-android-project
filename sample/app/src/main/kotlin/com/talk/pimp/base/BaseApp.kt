package com.talk.pimp.base

import android.app.Application
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import timber.log.Timber

open class BaseApp : Application() {

  override fun onCreate() {
    super.onCreate()

    Fabric.with(this, Crashlytics())

    Timber.plant(Timber.DebugTree())
    Timber.tag("pimp")
  }

}
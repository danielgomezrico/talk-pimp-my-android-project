package com.talk.pimp.screens

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.talk.pimp.R
import com.talk.pimp.api.repositories.user.UserRepository
import com.talk.pimp.utils.extensions.composeForIoTasks
import kotlinx.android.synthetic.main.welcome_view.*

class WelcomeActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.welcome_view)

    UserRepository().get()
        .composeForIoTasks()
        .subscribe({
          it.body?.let { userTextView.text = "${it.id}\n${it.email}\n${it.firstName}" }
        }, {
          Toast.makeText(this@WelcomeActivity, it.message, Toast.LENGTH_SHORT).show()
        })
  }
}

package com.talk.pimp.utils.extensions

import rx.Observable
import rx.Observable.Transformer
import rx.android.schedulers.AndroidSchedulers.mainThread
import rx.schedulers.Schedulers.io

/**
 * Shorthand to set [subscribeOn] and [observeOn] thread for observables,
 * show/hide progress dialogs when needed and retry requests on timeouts.
 */
fun <T> Observable<T>.composeForIoTasks(): Observable<T> = compose<T>(Transformer {
  it.subscribeOn(io()).observeOn(mainThread())
})
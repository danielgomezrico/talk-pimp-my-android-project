package com.talk.pimp

import com.talk.pimp.api.MyApi
import com.talk.pimp.api.utils.MyApiLog
import com.talk.pimp.base.BaseApp
import timber.log.Timber

/**
 * Release version of MyApp to enable fabric, implant timber crashlytics tree and setup
 * WinWin api.
 */
class MyApp : BaseApp() {

  override fun onCreate() {
    super.onCreate()
    MyApi.init("http://my-api.com", MyApiLog.LEVEL_NONE)
  }
}
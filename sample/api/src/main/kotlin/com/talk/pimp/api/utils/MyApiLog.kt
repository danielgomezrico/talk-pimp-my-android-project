package com.talk.pimp.api.utils

import android.support.annotation.IntDef

object MyApiLog {
  const val TAG = "pimp-api"
  const val LEVEL_NONE = 0L
  const val LEVEL_BASIC = 1L
  const val LEVEL_FULL = 2L

  @IntDef(LEVEL_NONE, LEVEL_BASIC, LEVEL_FULL)
  @Retention(AnnotationRetention.SOURCE)
  annotation class Level
}
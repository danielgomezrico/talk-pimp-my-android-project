package com.talk.pimp.api

/**
 * Proxy to read, parse and organize backend responses and errors
 *
 * @property body null if no there was a weird error (some kind of unexpected exception)
 * @property error null if no error
 */
open class Answer<out T>(val body: T? = null, val error: Throwable? = null)
#!/usr/bin/env sh
#
# Test run unit tests
#

:${GRADLEW:=gradlew}

if [ -z "$IS_CI_SERVER" ] ; then
  ./$GRADLEW clean testDebugUnitTestCoverage lint \
              --parallel --stacktrace -PdisablePreDex --rerun-tasks --no-daemon
else
  ./$GRADLEW testDebugUnitTestCoverage \
              --parallel --daemon
fi
#!/usr/bin/env bash
#
# Create the hash needed for facebook login for release and debug keystores
# Note: path to file should be absolute.
#

KEYSTORE_FOLDER=/Users/danielgomez22/barista/winwin-android/app

echo "--> Debug keystore:"
keytool -exportcert \
        -alias "androiddebugkey" \
        -storepass "android" \
        -keypass "android" \
        -keystore $KEYSTORE_FOLDER/debug.keystore \
        | openssl sha1 -binary | openssl base64

echo "--> Release keystore:"
keytool -exportcert -alias thewinwin -keystore $KEYSTORE_FOLDER/release.keystore | openssl sha1 -binary | openssl base64
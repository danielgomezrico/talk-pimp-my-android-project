#!/usr/bin/env bash
#
# Release Staging and Production apk to fabric.
#
: ${GRADLEW:=gradlew}
: ${APP_FOLDER:=app}

LOCAL_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

sh $LOCAL_PATH/build-staging.sh
sh $LOCAL_PATH/build-production.sh

if [ -z $IS_CI_SERVER ] ; then
  # If IS_CI_SERVER is not set...
  ./$GRADLEW crashlyticsUploadDistributionDebug --daemon --parallel
  ./$GRADLEW crashlyticsUploadDistributionRelease --daemon --parallel
else
  ./$GRADLEW crashlyticsUploadDistributionDebug --stacktrace -PdisablePreDex --no-daemon -Dscan
  ./$GRADLEW crashlyticsUploadDistributionRelease --stacktrace -PdisablePreDex --no-daemon -Dscan
fi